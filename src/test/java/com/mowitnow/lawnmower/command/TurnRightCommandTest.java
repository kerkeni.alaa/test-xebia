package com.mowitnow.lawnmower.command;

import com.mowitnow.lawnmower.domain.LawnMower;
import com.mowitnow.lawnmower.domain.LawnMowerOrientation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TurnRightCommandTest {

    LawnMower lawnMower;
    LawnMowerCommand lawnMowerCommand;

    @Before
    public void setUp(){
        lawnMower = new LawnMower(5,5, LawnMowerOrientation.N);
        lawnMowerCommand = new TurnRightCommand(lawnMower);
    }

    @Test
    public void LawnMower_should_turn_right(){
        //ACT
        lawnMowerCommand.execute();
        //ASSERT
        Assert.assertEquals(LawnMowerOrientation.E, lawnMower.getOrientation());

        //ACT
        lawnMowerCommand.execute();
        //ASSERT
        Assert.assertEquals(LawnMowerOrientation.S, lawnMower.getOrientation());

        //ACT
        lawnMowerCommand.execute();
        //ASSERT
        Assert.assertEquals(LawnMowerOrientation.W, lawnMower.getOrientation());

        //ACT
        lawnMowerCommand.execute();
        //ASSERT
        Assert.assertEquals(LawnMowerOrientation.N, lawnMower.getOrientation());
    }

}
