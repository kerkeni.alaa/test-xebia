package com.mowitnow.lawnmower.command;

import com.mowitnow.lawnmower.domain.Lawn;
import com.mowitnow.lawnmower.domain.LawnMower;
import com.mowitnow.lawnmower.domain.LawnMowerOrientation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ForwardCommandTest {

    Lawn lawn;
    LawnMower lawnMower;
    LawnMowerCommand lawnMowerCommand;

    @Before
    public void setUp(){
        lawn = new Lawn(5,5);
    }

    @Test
    public void LawnMower_x_axis_forward_should_OK(){
        //ARRANGE
        lawnMower = new LawnMower(0,0, LawnMowerOrientation.E);
        lawnMowerCommand = new ForwardCommand(lawn, lawnMower);
        //ACT
        lawnMowerCommand.execute();
        //ASSERT
        Assert.assertEquals(1, lawnMower.getxPosition());
        Assert.assertEquals(0, lawnMower.getyPosition());
        Assert.assertEquals(LawnMowerOrientation.E, lawnMower.getOrientation());

        //ARRANGE
        lawnMower = new LawnMower(2,0, LawnMowerOrientation.W);
        lawnMowerCommand = new ForwardCommand(lawn, lawnMower);
        //ACT
        lawnMowerCommand.execute();
        //ASSERT
        Assert.assertEquals(1, lawnMower.getxPosition());
        Assert.assertEquals(0, lawnMower.getyPosition());
        Assert.assertEquals(LawnMowerOrientation.W, lawnMower.getOrientation());
    }

    @Test
    public void LawnMower_y_axis_forward_should_OK(){
        //ARRANGE
        lawnMower = new LawnMower(0,0, LawnMowerOrientation.N);
        lawnMowerCommand = new ForwardCommand(lawn, lawnMower);
        //ACT
        lawnMowerCommand.execute();
        //ASSERT
        Assert.assertEquals(0, lawnMower.getxPosition());
        Assert.assertEquals(1, lawnMower.getyPosition());
        Assert.assertEquals(LawnMowerOrientation.N, lawnMower.getOrientation());

        //ARRANGE
        lawnMower = new LawnMower(2,2, LawnMowerOrientation.S);
        lawnMowerCommand = new ForwardCommand(lawn, lawnMower);
        //ACT
        lawnMowerCommand.execute();
        //ASSERT
        Assert.assertEquals(2, lawnMower.getxPosition());
        Assert.assertEquals(1, lawnMower.getyPosition());
        Assert.assertEquals(LawnMowerOrientation.S, lawnMower.getOrientation());
    }

    @Test
    public void LawnMower_should_not_move(){
        //ARRANGE
        lawnMower = new LawnMower(5,5, LawnMowerOrientation.N);
        lawnMowerCommand = new ForwardCommand(lawn, lawnMower);
        //ACT
        lawnMowerCommand.execute();
        //ASSERT
        Assert.assertEquals(5, lawnMower.getxPosition());
        Assert.assertEquals(5, lawnMower.getyPosition());
        Assert.assertEquals(LawnMowerOrientation.N, lawnMower.getOrientation());

        //ARRANGE
        lawnMower = new LawnMower(5,5, LawnMowerOrientation.E);
        lawnMowerCommand = new ForwardCommand(lawn, lawnMower);
        //ACT
        lawnMowerCommand.execute();
        //ASSERT
        Assert.assertEquals(5, lawnMower.getxPosition());
        Assert.assertEquals(5, lawnMower.getyPosition());
        Assert.assertEquals(LawnMowerOrientation.E, lawnMower.getOrientation());
    }

}