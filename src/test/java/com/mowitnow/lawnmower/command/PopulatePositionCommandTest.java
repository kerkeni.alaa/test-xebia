package com.mowitnow.lawnmower.command;

import com.mowitnow.lawnmower.domain.LawnMower;
import com.mowitnow.lawnmower.domain.LawnMowerOrientation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class PopulatePositionCommandTest {

    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    LawnMower lawnMower;
    LawnMowerCommand lawnMowerCommand;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
        lawnMower = new LawnMower(5,5, LawnMowerOrientation.N);
        lawnMowerCommand = new PopulatePositionCommand(lawnMower);
    }

    @Test
    public void LawnMower_should_populate_position() {
        //ACT
        lawnMowerCommand.execute();
        //ASSERT
        Assert.assertEquals("5 5 N", outputStreamCaptor.toString()
                .trim());
    }

}
