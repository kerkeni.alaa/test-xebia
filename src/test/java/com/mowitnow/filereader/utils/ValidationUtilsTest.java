package com.mowitnow.filereader.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

public class ValidationUtilsTest {

    Random random = new Random();

    @Test
    public void isPositiveInteger_should_OK(){
        //ASSERT
        Assert.assertTrue(ValidationUtils.isPositiveInteger("" + random.nextInt(Integer.MAX_VALUE)));
        Assert.assertTrue(ValidationUtils.isPositiveInteger("0"));
        Assert.assertFalse(ValidationUtils.isPositiveInteger("" + random.nextInt(Integer.MAX_VALUE) * -1));
    }

    @Test
    public void isValidOrientation_should_return_true(){
        //ARRANGE
        String valid = "NSEW";
        char validChar = valid.charAt(random.nextInt(valid.length()));
        //ASSERT
        Assert.assertTrue(ValidationUtils.isValidOrientation("" + validChar));
    }

    @Test
    public void isValidOrientation_should_return_false(){
        //ARRANGE
        String valid = "ABCDFGHIJKLMOPQRTUVXYZ";
        char validChar = valid.charAt(random.nextInt(valid.length()));
        //ASSERT
        Assert.assertFalse(ValidationUtils.isValidOrientation("" + validChar));
    }

}
