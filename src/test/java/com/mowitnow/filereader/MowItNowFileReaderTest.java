package com.mowitnow.filereader;

import com.mowitnow.filereader.domain.MowItNowFileContent;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Objects;

public class MowItNowFileReaderTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void getFileContent_should_retrun_MowItNowFileContent() throws URISyntaxException, MowItNowFileReaderException {
        //ARRANGE
        URL sampleFileUrl = MowItNowFileReaderTest.class.getClassLoader().getResource("filereader/sample");
        //ACT
        MowItNowFileContent content = MowItNowFileReader.getFileContent(Paths.get(Objects.requireNonNull(sampleFileUrl).toURI()).toString());
        //ASSERT
        Assert.assertEquals(5, content.getLawnData().getxLimit());
        Assert.assertEquals(5, content.getLawnData().getyLimit());
    }

    @Test
    public void getFileContent_should_detect_lawn_line_error() throws URISyntaxException, MowItNowFileReaderException {
        //ARRANGE
        URL sampleFileUrl = MowItNowFileReaderTest.class.getClassLoader().getResource("filereader/line1_corrupted_sample");
        //ASSERT
        expectedException.expect(MowItNowFileReaderException.class);
        expectedException.expectMessage("error at line 1: Lawn line must follow [number space number] pattern");
        //ACT
        MowItNowFileReader.getFileContent(Paths.get(Objects.requireNonNull(sampleFileUrl).toURI()).toString());
    }

    @Test
    public void getFileContent_should_detect_lawnmower_line_error() throws URISyntaxException, MowItNowFileReaderException {
        //ARRANGE
        URL sampleFileUrl = MowItNowFileReaderTest.class.getClassLoader().getResource("filereader/line2_corrupted_sample");
        //ASSERT
        expectedException.expect(MowItNowFileReaderException.class);
        expectedException.expectMessage("error at line 2: Lawnmower position line must follow [number space number space orientation] pattern");
        //ACT
        MowItNowFileReader.getFileContent(Paths.get(Objects.requireNonNull(sampleFileUrl).toURI()).toString());
    }

    @Test
    public void getFileContent_should_detect_commands_line_error() throws URISyntaxException, MowItNowFileReaderException {
        //ARRANGE
        URL sampleFileUrl = MowItNowFileReaderTest.class.getClassLoader().getResource("filereader/line5_corrupted_sample");
        //ASSERT
        expectedException.expect(MowItNowFileReaderException.class);
        expectedException.expectMessage("error at line 5: Commands line must contain only A G D characters");
        //ACT
        MowItNowFileReader.getFileContent(Paths.get(Objects.requireNonNull(sampleFileUrl).toURI()).toString());
    }

}