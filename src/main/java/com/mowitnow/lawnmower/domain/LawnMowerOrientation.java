package com.mowitnow.lawnmower.domain;

public enum LawnMowerOrientation {

    N,//north
    S,//south
    E,//est
    W //west

}
