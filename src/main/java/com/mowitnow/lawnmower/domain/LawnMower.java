package com.mowitnow.lawnmower.domain;

public class LawnMower {

    private int xPosition;
    private int yPosition;
    private LawnMowerOrientation orientation;

    public LawnMower(int xPosition, int yPosition, LawnMowerOrientation orientation) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.orientation = orientation;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public LawnMowerOrientation getOrientation() {
        return orientation;
    }

    public void setOrientation(LawnMowerOrientation orientation) {
        this.orientation = orientation;
    }

    @Override
    public String toString() {
        return "LawnMower{" +
                "xPosition=" + xPosition +
                ", yPosition=" + yPosition +
                ", orientation=" + orientation +
                '}';
    }
}
