package com.mowitnow.lawnmower;

import com.mowitnow.lawnmower.command.LawnMowerCommand;

import java.util.List;

public class LawnMowerCommandInvoker {

    private List<LawnMowerCommand> lawnMowerCommands;

    public LawnMowerCommandInvoker(List<LawnMowerCommand> lawnMowerCommands) {
        this.lawnMowerCommands = lawnMowerCommands;
    }

    public void executeCommands() {
        for (LawnMowerCommand lawnMowerCommand:lawnMowerCommands) {
            lawnMowerCommand.execute();
        }
    }

}
