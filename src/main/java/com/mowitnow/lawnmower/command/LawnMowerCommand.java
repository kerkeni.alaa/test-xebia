package com.mowitnow.lawnmower.command;

public interface LawnMowerCommand {

    void execute();

}
