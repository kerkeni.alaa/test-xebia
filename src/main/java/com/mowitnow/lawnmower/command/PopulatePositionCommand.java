package com.mowitnow.lawnmower.command;

import com.mowitnow.lawnmower.domain.LawnMower;

public class PopulatePositionCommand implements LawnMowerCommand{

    private final LawnMower lawnMower;

    public PopulatePositionCommand(LawnMower lawnMower) {
        this.lawnMower = lawnMower;
    }

    @Override
    public void execute() {
        String position = this.lawnMower.getxPosition() +
                " " +
                this.lawnMower.getyPosition() +
                " " +
                this.lawnMower.getOrientation().name();
        System.out.println(position);
    }

}
