package com.mowitnow.lawnmower.command;

import com.mowitnow.lawnmower.domain.LawnMower;
import com.mowitnow.lawnmower.domain.LawnMowerOrientation;

public class TurnLeftCommand implements LawnMowerCommand {

    private final LawnMower lawnMower;

    public TurnLeftCommand(LawnMower lawnMower) {
        this.lawnMower = lawnMower;
    }

    @Override
    public void execute() {
        switch (this.lawnMower.getOrientation()) {
            case N:
                this.lawnMower.setOrientation(LawnMowerOrientation.W);
                break;
            case W:
                this.lawnMower.setOrientation(LawnMowerOrientation.S);
                break;
            case S:
                this.lawnMower.setOrientation(LawnMowerOrientation.E);
                break;
            case E:
                this.lawnMower.setOrientation(LawnMowerOrientation.N);
                break;
        }
    }

}
