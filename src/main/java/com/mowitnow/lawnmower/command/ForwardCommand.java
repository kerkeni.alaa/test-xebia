package com.mowitnow.lawnmower.command;

import com.mowitnow.lawnmower.domain.Lawn;
import com.mowitnow.lawnmower.domain.LawnMower;

public class ForwardCommand implements LawnMowerCommand {

    private final Lawn lawn;
    private final LawnMower lawnMower;

    public ForwardCommand(Lawn lawn, LawnMower lawnMower) {
        this.lawnMower = lawnMower;
        this.lawn = lawn;
    }

    @Override
    public void execute() {
        switch (this.lawnMower.getOrientation()) {
            case N:
                if (this.lawnMower.getyPosition() < this.lawn.getyLimit())
                    this.lawnMower.setyPosition(this.lawnMower.getyPosition() + 1);
                break;
            case E:
                if (this.lawnMower.getxPosition() < this.lawn.getxLimit())
                    this.lawnMower.setxPosition(this.lawnMower.getxPosition() + 1);
                break;
            case S:
                if (this.lawnMower.getyPosition() > 0)
                    this.lawnMower.setyPosition(this.lawnMower.getyPosition() - 1);
                break;
            case W:
                if (this.lawnMower.getxPosition() > 0)
                    this.lawnMower.setxPosition(this.lawnMower.getxPosition() - 1);
                break;
        }
    }

}
