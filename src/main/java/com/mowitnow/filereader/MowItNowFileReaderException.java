package com.mowitnow.filereader;

public class MowItNowFileReaderException extends Exception {

    public MowItNowFileReaderException(String message) {
        super(message);
    }

}
