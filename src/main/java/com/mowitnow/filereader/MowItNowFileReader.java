package com.mowitnow.filereader;

import com.mowitnow.filereader.domain.CommandData;
import com.mowitnow.filereader.domain.LawnData;
import com.mowitnow.filereader.domain.LawnMowerData;
import com.mowitnow.filereader.domain.MowItNowFileContent;
import com.mowitnow.lawnmower.domain.LawnMowerOrientation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.mowitnow.filereader.utils.ValidationUtils.isPositiveInteger;
import static com.mowitnow.filereader.utils.ValidationUtils.isValidOrientation;

public class MowItNowFileReader {

    public static MowItNowFileContent getFileContent(String path) throws MowItNowFileReaderException {
        try {
            List<String> allLines = Files.readAllLines(Paths.get(path));
            if(!allLines.isEmpty()){
                validateLawnDataLine(allLines.get(0));
                LawnData lawnData = parseLawnData(allLines.get(0));

                List<LawnMowerData> lawnMowerData = new ArrayList<>();
                for (int i = 1; i < allLines.size(); i += 2) {
                    validateLawnMowerLine(allLines.get(i) , i);
                    validateCommandsLine(allLines.get(i+1) , i + 1);
                    lawnMowerData.add(parseLawnMowerAndCommands(allLines.get(i), allLines.get(i + 1)));
                }
                return new MowItNowFileContent(lawnData, lawnMowerData);
            }else{
                throw new MowItNowFileReaderException("File is empty");
            }
        } catch (IOException e) {
            throw new MowItNowFileReaderException("please check file : " + e.getMessage());
        }
    }

    private static LawnData parseLawnData(String s)  {
        String[] lawns = s.split(" ");
        return new LawnData(Integer.parseInt(lawns[0]), Integer.parseInt(lawns[1]));
    }

    private static LawnMowerData parseLawnMowerAndCommands(String lawnMowerLine, String commandsLine) {
        String[] lawnMowerLineParams = lawnMowerLine.split(" ");

        List<Character> commandsParams = commandsLine.chars().mapToObj(e -> (char) e).collect(Collectors.toList());
        List<CommandData> commandsData = commandsParams.stream()
                .map(character -> CommandData.valueOf(character.toString())).collect(Collectors.toList());

        return new LawnMowerData(Integer.parseInt(lawnMowerLineParams[0]), Integer.parseInt(lawnMowerLineParams[1]),
                LawnMowerOrientation.valueOf(lawnMowerLineParams[2]), commandsData);
    }

    private static void validateLawnDataLine(String lawnLine) throws MowItNowFileReaderException {
        String[] params = lawnLine.split(" ");
        if (!isPositiveInteger(params[0]) ||
                !isPositiveInteger(params[1])) {
            throw new MowItNowFileReaderException("error at line 1: Lawn line must follow [number space number] pattern");
        }
    }

    private static void validateLawnMowerLine(String lawnMowerLine, int lineIndex) throws MowItNowFileReaderException {
        String[] lawnMowerLineParams = lawnMowerLine.split(" ");
        if (!isPositiveInteger(lawnMowerLineParams[0]) ||
                !isPositiveInteger(lawnMowerLineParams[1]) ||
                !isValidOrientation(lawnMowerLineParams[2])) {
            throw new MowItNowFileReaderException("error at line " + ++lineIndex +
                    ": Lawnmower position line must follow [number space number space orientation] pattern");
        }
    }

    private static void validateCommandsLine(String commandsLine, int lineIndex) throws MowItNowFileReaderException {
        Set<Character> commandsParams = commandsLine.chars().mapToObj(e -> (char) e).collect(Collectors.toSet());
        commandsParams.remove('A');
        commandsParams.remove('G');
        commandsParams.remove('D');
        if (commandsParams.size() > 0) {
            throw new MowItNowFileReaderException("error at line " + ++lineIndex +
                    ": Commands line must contain only A G D characters");
        }
    }

}
