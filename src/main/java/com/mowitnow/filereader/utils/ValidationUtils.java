package com.mowitnow.filereader.utils;

public class ValidationUtils {

    public static boolean isPositiveInteger(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            int i = Integer.parseInt(strNum);
            return i >= 0;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static boolean isValidOrientation(String orientation) {
        if (orientation == null) {
            return false;
        }
        return "N".equals(orientation) ||
                "S".equals(orientation) ||
                "E".equals(orientation) ||
                "W".equals(orientation);
    }

}
