package com.mowitnow.filereader.domain;

import java.util.List;

public class MowItNowFileContent {

    private final LawnData lawnData;
    private final List <LawnMowerData> lawnMowerData;

    public MowItNowFileContent(LawnData lawnData, List<LawnMowerData> lawnMowerData) {
        this.lawnData = lawnData;
        this.lawnMowerData = lawnMowerData;
    }

    public LawnData getLawnData() {
        return lawnData;
    }

    public List<LawnMowerData> getLawnMowerData() {
        return lawnMowerData;
    }

}
