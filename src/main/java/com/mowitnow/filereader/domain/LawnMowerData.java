package com.mowitnow.filereader.domain;

import com.mowitnow.lawnmower.domain.LawnMowerOrientation;

import java.util.List;

public class LawnMowerData {

    private int xPosition;
    private int yPosition;
    private LawnMowerOrientation orientation;
    private List<CommandData> lawnMowerCommands;

    public LawnMowerData(int xPosition, int yPosition, LawnMowerOrientation orientation,
                         List<CommandData> lawnMowerCommands) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.orientation = orientation;
        this.lawnMowerCommands = lawnMowerCommands;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public LawnMowerOrientation getOrientation() {
        return orientation;
    }

    public void setOrientation(LawnMowerOrientation orientation) {
        this.orientation = orientation;
    }

    public List<CommandData> getLawnMowerCommands() {
        return lawnMowerCommands;
    }

    public void setLawnMowerCommands(List<CommandData> lawnMowerCommands) {
        this.lawnMowerCommands = lawnMowerCommands;
    }
}
