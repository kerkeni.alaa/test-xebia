package com.mowitnow.filereader.domain;

public class LawnData {

    private final int xLimit;
    private final int yLimit;

    public LawnData(int xLimit, int yLimit) {
        this.xLimit = xLimit;
        this.yLimit = yLimit;
    }

    public int getxLimit() {
        return xLimit;
    }

    public int getyLimit() {
        return yLimit;
    }

    @Override
    public String toString() {
        return "Lawn{" +
                "xLimit=" + xLimit +
                ", yLimit=" + yLimit +
                '}';
    }

}
