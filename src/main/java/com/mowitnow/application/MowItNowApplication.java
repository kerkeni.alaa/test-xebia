package com.mowitnow.application;

import com.mowitnow.application.mappers.LawnMowerDataMapper;
import com.mowitnow.filereader.MowItNowFileReader;
import com.mowitnow.filereader.MowItNowFileReaderException;
import com.mowitnow.filereader.domain.CommandData;
import com.mowitnow.filereader.domain.MowItNowFileContent;
import com.mowitnow.lawnmower.LawnMowerCommandInvoker;
import com.mowitnow.lawnmower.command.*;
import com.mowitnow.lawnmower.domain.Lawn;
import com.mowitnow.lawnmower.domain.LawnMower;

import java.util.ArrayList;
import java.util.List;

public class MowItNowApplication {

    public static void main(String[] args) {
        try {
            if(args.length == 0) {
                throw new MowItNowFileReaderException("Please add input file absolute path as an argument");
            }
            String inputFilePath = args[0];
            MowItNowFileContent content = MowItNowFileReader.getFileContent(inputFilePath);
            Lawn lawn = LawnMowerDataMapper.lawnDataToLawn(content.getLawnData());
            System.out.println("INFO| file parsed successfully, result : ");
            content.getLawnMowerData().forEach(lawnMowerData -> {
                List<LawnMowerCommand> lawnMowerCommands = new ArrayList<>();
                LawnMower lawnMower = LawnMowerDataMapper.LawnMowerDataToLawnMowerData(lawnMowerData);
                lawnMowerData.getLawnMowerCommands().forEach(lawnMowerCommandEnum -> {
                    if (lawnMowerCommandEnum == CommandData.D) {
                        lawnMowerCommands.add(new TurnRightCommand(lawnMower));
                    }
                    if (lawnMowerCommandEnum == CommandData.G) {
                        lawnMowerCommands.add(new TurnLeftCommand(lawnMower));
                    }
                    if (lawnMowerCommandEnum == CommandData.A) {
                        lawnMowerCommands.add(new ForwardCommand(lawn, lawnMower));
                    }
                });
                lawnMowerCommands.add(new PopulatePositionCommand(lawnMower));
                LawnMowerCommandInvoker invoker = new LawnMowerCommandInvoker(lawnMowerCommands);
                invoker.executeCommands();
            });
        } catch (MowItNowFileReaderException e) {
            System.out.println("ERROR| " + e.getMessage());
        }
    }

}
