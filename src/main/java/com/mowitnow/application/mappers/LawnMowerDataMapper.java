package com.mowitnow.application.mappers;

import com.mowitnow.filereader.domain.LawnData;
import com.mowitnow.filereader.domain.LawnMowerData;
import com.mowitnow.lawnmower.domain.Lawn;
import com.mowitnow.lawnmower.domain.LawnMower;

public class LawnMowerDataMapper {

    public static Lawn lawnDataToLawn(LawnData lawnData){
        if(lawnData == null) {
            return null;
        }
        return new Lawn(lawnData.getxLimit(),lawnData.getyLimit());
    }

    public static LawnMower LawnMowerDataToLawnMowerData(LawnMowerData lawnMowerData){
        if(lawnMowerData == null) {
            return null;
        }
        return new LawnMower(lawnMowerData.getxPosition(),lawnMowerData.getyPosition(),lawnMowerData.getOrientation());
    }

}
